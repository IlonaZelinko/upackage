var gulp = require('gulp');
var sass = require('gulp-sass');
var ts = require('gulp-typescript');

var tsProject = ts.createProject({
    declaration: true,
});

var paths = { 
    scripts: [],
    ts: {
        src: ['assets/ts/**/*.ts'],
        dst: 'web/js'
    },
    styles: {
        src: ['assets/sass/**/*.scss'],
        dst: 'web/css'
    }
};

gulp.task('styles', function() {
    gulp.src(paths.styles.src)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(paths.styles.dst));
});

gulp.task('ts', function() {
    var tsResult = gulp.src(paths.ts.src)
    .pipe(tsProject());

    return tsResult.js.pipe(gulp.dest(paths.ts.dst));
});

gulp.task('watch', function() {
    gulp.watch(paths.styles.src, ['styles']);
    gulp.watch(paths.ts.src, ['ts']);
});

gulp.task('default', ['styles', 'ts', 'watch']);
